import java.awt.*;
import javax.swing.*;
import javax.swing.text.*;
import java.awt.event.*;
import java.util.*;
/*************************************************************
 * GUI for the Adventure Game
 * 
 * @author Scott Grissom
 *         w/ edits by Kyle Hekhuis
 * @version October 7, 2011
 ************************************************************/
public class GUI extends JPanel{

    /** the analyzer that does all the real work */
    Game myGame;
    
    /** Buttons to initiate each action */
    JButton north, east, south, west, take, drop, look, help, 
            inventory, use, leave, time;
    
    /** GUI frame */
    JFrame myGUI;
    JTextArea results;
    
    
    /** menu items */
    // if attempting the challenge activity
    JMenuBar menus;
    JMenu fileMenu;
    JMenuItem quitItem, newItem;
    
    /*****************************************************************
     * Main Method
     ****************************************************************/ 
        public static void main(String args[]){
            new GUI();
      }
        
    /*****************************************************************
     * constructor installs all of the GUI components
     ****************************************************************/    
        public GUI(){
            
        // establish the frame
        myGUI = new JFrame();
        myGUI.setSize(600,300);
        myGUI.setTitle("Escape Hiroshima");    
        
        // create display area
        results = new JTextArea(30,60);
        JScrollPane scrollPane = new JScrollPane(results);
        myGUI.add(BorderLayout.CENTER, scrollPane);
    
        // create data entry panel
        help = new JButton("Help");
        take = new JButton("Take");
        use = new JButton("Use");
        drop = new JButton("Drop");
        look = new JButton("Look");
        inventory = new JButton("Inventory");
        time = new JButton("Time");
        leave = new JButton("Leave");
        
        JPanel actionPanel = new JPanel();
        actionPanel.add(new JLabel("Actions: "));
        actionPanel.add(help);
        actionPanel.add(take);
        actionPanel.add(use);
        actionPanel.add(drop);
        actionPanel.add(look); 
        actionPanel.add(inventory); 
        actionPanel.add(time);
        actionPanel.add(leave);
    
        myGUI.add(BorderLayout.SOUTH, actionPanel);
        
        // create button panel
        JPanel directionPanel = new JPanel();
        directionPanel.setLayout(new BoxLayout(directionPanel, BoxLayout.Y_AXIS));
        north = new JButton("North");
        south = new JButton("South");
        east = new JButton("East");
        west = new JButton("West");
        directionPanel.add(new JLabel("Directions"));
        directionPanel.add(north);
        directionPanel.add(south);
        directionPanel.add(east);
        directionPanel.add(west);
        myGUI.add(BorderLayout.EAST, directionPanel);
        
        // register the listeners
        ButtonListener listener = new ButtonListener();
        help.addActionListener(listener);
        take.addActionListener(listener);
        use.addActionListener(listener);
        drop.addActionListener(listener);
        look.addActionListener(listener);
        inventory.addActionListener(listener);
        time.addActionListener(listener);
        leave.addActionListener(listener);
        north.addActionListener(listener);
        south.addActionListener(listener);
        east.addActionListener(listener);
        west.addActionListener(listener);
        
        // set up File menu
        fileMenu = new JMenu("File");
        quitItem = new JMenuItem("Quit");
        newItem = new JMenuItem("New Game");
        fileMenu.add(newItem);
        fileMenu.add(quitItem);
        menus = new JMenuBar();
        myGUI.setJMenuBar(menus);
        menus.add(fileMenu);
        
        // register the menu items with the action listener
        quitItem.addActionListener(listener);
        newItem.addActionListener(listener);
        
        myGUI.setVisible(true);
        myGUI.pack();
        
        newGame();
        }

    private void gameOver()
    {
        results.append("\n\n" + myGame.getMessage());   
        help.setEnabled(false);
        take.setEnabled(false);
        use.setEnabled(false);
        drop.setEnabled(false);
        look.setEnabled(false);
        inventory.setEnabled(false);
        time.setEnabled(false);
        leave.setEnabled(false);
        north.setEnabled(false);
        south.setEnabled(false);
        east.setEnabled(false);
        west.setEnabled(false);
    }
    
    private void newGame()
    {
        myGame = new Game();
        results.setText(myGame.getMessage());
        help.setEnabled(true);
        take.setEnabled(true);
        use.setEnabled(true);
        drop.setEnabled(true);
        look.setEnabled(true);
        inventory.setEnabled(true);
        time.setEnabled(true);
        leave.setEnabled(true);
        north.setEnabled(true);
        south.setEnabled(true);
        east.setEnabled(true);
        west.setEnabled(true);
    }

    /*****************************************************************
     * This method is called when any button is clicked.  The proper
     * internal method is called as needed.
     * 
     * @param e the event that was fired
     ****************************************************************/       
    private class ButtonListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
        
            // extract the button that was clicked
            JComponent buttonPressed = (JComponent) e.getSource();
            
            if (buttonPressed == quitItem){
                System.exit(1);
            }else if (buttonPressed == newItem){
                newGame();
            }else if (buttonPressed == east){
                myGame.move("east");
            }else if (buttonPressed == west){
                myGame.move("west");
            }else if (buttonPressed == north){
                myGame.move("north");
            }else if (buttonPressed == south){
                myGame.move("south");
            }else if (buttonPressed == take){
                myGame.take();
            }else if (buttonPressed == look){
                myGame.look();
            }else if (buttonPressed == inventory){
                myGame.show();
            }else if (buttonPressed == help){
                myGame.help();
            }else if (buttonPressed == drop){
                String item = JOptionPane.showInputDialog(null, "What do you want to drop?");
                if (item != null)
                    myGame.drop(item);    
            }else if (buttonPressed == use){
                String item = JOptionPane.showInputDialog(null, "What do you want to use?");
                if (item != null)
                    myGame.use(item);  
            }else if (buttonPressed == leave){
                myGame.leave();
            }else if (buttonPressed == time){
                myGame.checkTime();
            }
            
            results.append("\n\n" + myGame.getMessage());   
            
            if (myGame.gameOver()){
                    gameOver();
            }
        }
    }
}