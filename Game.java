import java.util.ArrayList;
import java.util.Random;
/**
 * Creates an instance of the game.
 * 
 * @author Kyle Hekhuis 
 * @version Fall 2014
 */
public class Game
{
    // Instantiate instance variables
    private ArrayList<Item> inventory;
    private Item leaflet, motorcycle, knapsack, 
                 ticket, foodWater, map, money, key;
    private Room homeLivingRoom, homeBedroom, trainStation, 
                 trainStationTB, townSquare, store, 
                 parkingLot, road, lost, townFarAway, 
                 currentLocation;
    private String msg = "";
    private Time t;
    
    /**
     * Creates a game
     * Sets current location to Town Square
     * Sets difficulty
     */
    public Game()
    {
        createRooms();
        inventory = new ArrayList<Item>();
        currentLocation = townSquare;
        
        /*
         * Difficulty setting
         * ==================
         * 0.0 = Easy
         * 0.4 = Medium
         * .07 = Hard (Have to have go through perfectly)
         * >.07 = Impossible to win, will lose no matter what
         */
        t = new Time(.05);
        
        setWelcomeMessage();
    }
    
    /**
     * Instantiates the Rooms and Items. 
     * Identify all of the room neighbors. 
     */
    private void createRooms()
    {
        // Instantiate items
        leaflet = new Item("a leaflet", 
                           "a leaflet warning of a possible attack on Hiroshima by the US",
                           1);
        motorcycle = new Item("a motorcycle",
                              "a Japanese military motorcycle with a sidecar attached",
                              600);
        knapsack = new Item("a knapsack",
                            "a knapsack with your belongings you took from home",
                            10);
        ticket = new Item("a ticket",
                          "a ticket to board the train out of Hiroshima",
                          1);
        foodWater = new Item("food and water",
                             "food and water for when you get hungry or thirsty",
                             1);
        map = new Item("a map",
                       "a map of Japan",
                       1);
        money = new Item("money",
                         "money you can use to buy things",
                         1);
        key = new Item("a key",
                       "a key to some type of vehicle",
                       1);
                       
        // Instantiate rooms
        // Create temp ArrayList to pass as parameter when creating room.
        // Clear previous items then add the item needed for that room
        // every time you need to pass it as parameter.
        ArrayList<Item> temp = new ArrayList<Item>();
        temp.add(knapsack);
        
        homeLivingRoom = new Room("in the living room of your home. " + "\n" +
                                  "It is extremely messy, but you are " + "\n" +
                                  "able to find some of your belongings. " + "\n",
                                  temp);
        
        temp.clear();
        temp.add(money);
        
        homeBedroom = new Room("in the bedroom of your home. ",
                               temp);
                               
        temp.clear();
        temp.add(key);                       
                               
        trainStation = new Room("in the train station hoping to catch " + "\n" +
                                "a ride out of Hiroshima. There’s a " + "\n" +
                                "ticket booth that you can buy a ticket " + "\n" +
                                "for the train, however the line is long. " + "\n" +
                                "You need the ticket to get on though " + "\n" +
                                "as guards won’t allow anyone on the " + "\n" +
                                "train without one. " + "\n",
                                temp);
                                
        temp.clear();
        temp.add(ticket);                        
                                
        trainStationTB = new Room("at the ticket booth in the train station. " + "\n",
                                  temp);
                                           
        temp.clear();
        temp.add(leaflet);                                   
                                           
        townSquare = new Room("standing in the town square of Hiroshima. ",
                              temp);
                              
        temp.clear();
        temp.add(foodWater);
        temp.add(map);
                              
        store = new Room("at the local general store. The owners are " + "\n" +
                         "boarding it up, but they allow you to go " + "\n" +
                         "inside and shop. " + "\n",
                         temp);
                         
        temp.clear();
        temp.add(motorcycle);                 
                         
        parkingLot = new Room("standing in the parking lot outside of the " + "\n" +
                              "general store. " + "\n",
                              temp);
                              
        temp = null;
                              
        road = new Room("at the main road in town. You don’t travel much and " + "\n" +
                        "aren’t sure exactly where it goes. You do know though " + "\n" +
                        "that one way takes you directly out of the city, but " + "\n" +
                        "the other way will take you all through the city and " + "\n" +
                        "then out if you know the proper side road to take. ");
        lost = new Room("in the middle of no where because you went the wrong " + "\n" +
                        "direction on the road. ");
        townFarAway = new Room("in a town far away from the city of Hiroshima. " + "\n" +
                               "You stop at a building that has a radio and " + "\n" +
                               "hear that Hiroshima has just been leveled by " + "\n" +
                               "something called an Atomic Bomb. It’s a good " + "\n" +
                               "thing you made it out! ");
        
        // Add room neighbors
        homeLivingRoom.addNeighbor("north", townSquare);
        homeLivingRoom.addNeighbor("south", homeBedroom);
        
        homeBedroom.addNeighbor("north", homeLivingRoom);
        
        trainStation.addNeighbor("east", townSquare);
        trainStation.addNeighbor("west", trainStationTB);
        
        trainStationTB.addNeighbor("east", trainStation);
        
        townSquare.addNeighbor("north", parkingLot);
        townSquare.addNeighbor("east", road);
        townSquare.addNeighbor("south", homeLivingRoom);
        townSquare.addNeighbor("west", trainStation);
        
        store.addNeighbor("south", parkingLot);
        
        parkingLot.addNeighbor("north", store);
        parkingLot.addNeighbor("south", townSquare);
        
        road.addNeighbor("east", townFarAway);
        road.addNeighbor("south", lost);
    }
    
    /**
     * Initializes the game’s message with a description of the game.
     */
    private void setWelcomeMessage()
    {
        msg = "You are a Japanese man living in Hiroshima during World War 2. " + "\n" +
              "On August 1, 1945 and the United States military dropped " + "\n" +
              "leaflets letting people know Hiroshima could be one of the " + "\n" +
              "sites that is about to be bombed. You don’t find one of them " + "\n" +
              "until midday on August 5, 1945. Once you receive it you decide " + "\n" +
              "to get out of the city, but how? You must hurry, as there’s " + "\n" +
              "limited time to escape before the attack!";
    }
    
    public String getMessage()
    {
        return msg;
    }
    
    /**
     * Randomly selects a hint to give.
     */
    public void help()
    {
        Random r = new Random();
        int hintNumber = (r.nextInt(7) + 1);
        switch (hintNumber)
        {
            case 1: msg = "Perhaps some sort of vehicle will help you?";
                    break;
            case 2: msg = "You should probably have some food and water.";
                    break;
            case 3: msg = "A map could really help you find out where to go.";
                    break;
            case 4: msg = "You should bring a knapsack with your belongings along.";
                    break;
            case 5: msg = "It would be wise to head for the road.";
                    break;
            case 6: msg = "Things cost money to buy.";
                    break;
            case 7: msg = "If you can't take something, try using something on it!";
        }
    }
    
    /**
     * Update the game’s message with the current room’s long description.
     */
    public void look()
    {
        msg = currentLocation.getLongDescription();
    }
    
    /**
     * Moves current location in desired direction if it can.
     * If unable to, it lets you know.
     * Adds hour to time each move.
     */
    public void move(String direction)
    {
        Room nextRoom = currentLocation.getNeighbor(direction);
        if(nextRoom == null)
            msg = "You can't go in that direction";
        else
        {
            currentLocation = nextRoom;
            msg = currentLocation.getLongDescription();
        }
        t.addTime();
    }
    
    /**
     * Returns true if you are in the town far away with
     * enough time left or if you are lost (see 'Leave' method) 
     * or if player is out of time.
     */
    public boolean gameOver()
    {
        if(currentLocation == townFarAway && t.getTime() < 1.08)
        {
            msg = "Congratulations! You have won the game!";
            return true;
        }
        else if(currentLocation == lost || t.getTime() == 1.08)
        {
            msg = "An atomic bomb has exploded over Hiroshima, " + "\n" +
                  "destroying everything in the city, including you." + "\n" +
                  "You have lost.";
            return true;
        }
        else
            return false;
    }
    
    /**
     * Removes any items from the room and adds them
     * to inventory if they weight less than 50 lbs.
     * Special case for motorcycle (See 'Use' method).
     */
    public void take()
    {
        if(currentLocation.hasItem())
        {
            if(check(currentLocation.getItem()) == 1)
            {
                ArrayList<Item> temp = currentLocation.removeItem();
                for(Item i : temp)
                {
                    if(i.getWeight() < 50)
                    {
                        inventory.add(i);
                        msg = "The item(s) are now in your possession.";
                    }
                }
            }
            else if(check(currentLocation.getItem()) == 2)
                msg = "That item is too heavy to take.";
            else
                msg = "You can not take this object.";
        }
        else
            msg = "There is nothing here to take.";
    }
    
    /**
     * Check to make sure I can properly take items and
     * not accidently delete them forever upon failed 'take'
     * attempt.
     */
    private int check(ArrayList<Item> i)
    {
        int count = 0;
        ArrayList<Item> temp = i;
        for(Item j : temp)
        {
            if(j.getName().equals("a motorcycle"))
                count = 2;
            else if(!j.getName().equals("a map") &&
               !j.getName().equals("food and water") &&
               !j.getName().equals("a ticket"))
                count = 1;
        }
        return count;
    }
    
    /**
     * Checks inventory for item and returns it if found.
     */
    public Item checkForItem(String name)
    {
        for(Item i : inventory)
        {
            if(i.getName().equals(name))
                return i;
        }
        return null;
    }
    
    /**
     * Drops the desired item into the room you
     * are currently in if you have the item.
     */
    public void drop(String name)
    {
        if(checkForItem(name) == null)
            msg = "You are not holding that item.";
        else
            currentLocation.addItem(checkForItem(name));
    }
    
    /**
     * Update the game’s message with a list of all items
     * in the player's inventory.
     */
    public void show()
    {
        String temp = "";
        if(!inventory.isEmpty())
        {
            temp += "You have:" + "\n";
            for(Item i : inventory)
                temp += "-" + i.getName() + "\n";
        }
        else
            temp += "You don't have any items.";
        msg = temp;
    }
    
    /**
     * If you are in the 'road' room and you have the
     * motorcycle, knapsack, food and water, and a map
     * it allows you to leave to a town far away and win.
     * If you leave in the 'road' room and don't have the 
     * proper items in inventory, you get lost and lose.
     */
    public void leave()
    {
        if(currentLocation == road)
        {
            if(inventory.contains(motorcycle) &&
               inventory.contains(knapsack) &&
               inventory.contains(foodWater) &&
               inventory.contains(map))
            {
                move("east");
            }
            else
                move("south");
        }
        else
            msg = "You can not leave from here.";
    }
    
    /**
     * Check what the current time is in the game.
     */
    public void checkTime()
    {
        msg = t.toString();
    }
    
    /**
     * Use an item if able to
     */
    public void use(String name)
    {
        ArrayList<Item> temp;
        if(checkForItem("money") != null || checkForItem("a key") != null)
        {
            if(name.equals("money") && currentLocation == trainStationTB ||
               name.equals("money") && currentLocation == store ||
               name.equals("a key") && currentLocation == parkingLot)
            {
                temp = currentLocation.removeItem();
                for(Item i : temp)
                {
                    inventory.add(i);
                    msg = "The item(s) are now in your possession.";
                }
                if(name.equals("money"))
                    inventory.remove(money);
                else
                    inventory.remove(key);
            }
        }
        else
            msg = "You don't have that item to use.";
    }
    
    public static void main(String args[])
    {
        Game g = new Game();
        System.out.println(g.getMessage());
        g.move("south");
        System.out.println(g.getMessage());
        g.take();
        System.out.println(g.getMessage());
        g.move("south");
        System.out.println(g.getMessage());
        g.take();
        System.out.println(g.getMessage());
        g.move("north");
        System.out.println(g.getMessage());
        g.move("north");
        System.out.println(g.getMessage());
        g.move("west");
        System.out.println(g.getMessage());
        g.take();
        System.out.println(g.getMessage());
        g.move("east");
        System.out.println(g.getMessage());
        g.move("north");
        System.out.println(g.getMessage());
        g.move("north");
        System.out.println(g.getMessage());
        g.use("money");
        System.out.println(g.getMessage());
        g.move("south");
        System.out.println(g.getMessage());
        g.use("a key");
        System.out.println(g.getMessage());
        g.move("south");
        System.out.println(g.getMessage());
        g.move("east");
        System.out.println(g.getMessage());
        g.show();
        System.out.println(g.getMessage());
        g.leave();
        System.out.println(g.getMessage());
        if(g.gameOver())
            System.out.println(g.getMessage());
    }
    
    /*
     * To win game:
     * move("south");
     * take();
     * move("south");
     * take();
     * move("north");
     * move("north");
     * move("west");
     * take();
     * move("east");
     * move("north");
     * move("north");
     * use("money");
     * move("south");
     * use("a key");
     * move("south");
     * move("east");
     * leave();
     */
}
