/**
 * Class for making items
 * 
 * @author Kyle Hekhuis 
 * @version Fall 2014
 */
public class Item
{
    private String name;
    private String itemDescription;
    private int weight;

    /**
     * Constructor for objects of class Item
     * Parameters (String name, String itemDescription, int weight)
     * String name = name of object
     * String itemDescription = description of item
     * int weight = weight of item
     */
    public Item(String name, String itemDescription, int weight)
    {
        this.name = name;
        this.itemDescription = itemDescription;
        this.weight = weight;
    }
    
    public String getName()
    {
        return name;
    }
    
    public String getItemDescription()
    {
        return itemDescription;
    }
    
    public int getWeight()
    {
        return weight;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public void setItemDescription(String itemDescription)
    {
        this.itemDescription = itemDescription;
    }
    
    public void setWeight(int weight)
    {
        this.weight = weight;
    }
}