Text Adventure Game
Language: Java
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 163 - Java Programming 1
Semester / Year: Fall 2014

Creates a simple text adventure game. You are a person in Hiroshima before it
is about to have the atomic bomb dropped on it. The goal is to escape Hiroshima
before the bomb drops.

Usage:
Run the game with the main method in 'GUI'. Press the buttons for cardinal
directions to move around the map. Use the 'Take' button to pick up an item.
The 'Look' button tells you what items are available. The 'Inventory' button will
tell you what you have in your inventory. Using the 'Help' button will give you
random hints. You can drop or use an item with the 'Drop' and 'Use' buttons
respectively. To see how much time you have left to escape, use the 'Time' button.
Use the 'Leave' button to leave Hiroshima once you have met the requirements to leave.