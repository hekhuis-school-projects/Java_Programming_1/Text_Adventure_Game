import java.util.HashMap;
import java.util.ArrayList;
/**
 * Class to create objects of Room
 * that maintain information about the room
 * (description, items, adjacent rooms).
 * 
 * @author Kyle Hekhuis
 * @version Fall 2014
 */
public class Room
{
    // Instantiate instance variables
    private String roomDescription;
    private ArrayList<Item> item;
    private HashMap <String, Room> myNeighbors;
    
    /**
     * Constructor for objects of class Room
     * Parameter (String roomDescription)
     * String roomDescription = description of the room
     */
    public Room(String roomDescription)
    {
        this.roomDescription = roomDescription;
        item = null;
        myNeighbors = new HashMap<String, Room>();
    }
    
    /**
     * Overloaded constructor for objects of class Room
     * Parameters (String roomDescription, ArrayList<Item> i)
     * String roomDescription = description of the room
     * ArrayList<Item> i = items in the room
     */
    public Room(String roomDescription, ArrayList<Item> i)
    {
        this.roomDescription = roomDescription;
        item = new ArrayList<Item>();
        for(Item temp : i)
        {
        	item.add(temp);
        }
        item.trimToSize();
        myNeighbors = new HashMap<String, Room>();
    }
    
    public String getRoomDescription()
    {
        return roomDescription;
    }
    
    public ArrayList<Item> getItem()
    {
        return item;
    }
    
    public void addItem(Item i)
    {
        item.add(i);
    }
    
    /**
     * Return true if the room has an item.
     */
    public boolean hasItem()
    {
        return getItem() != null;
    }
    
    /**
     * Add the provided room and corresponding 
     * direction to the HashMap of neighbors.
     */
    public void addNeighbor(String pDirection, Room r)
    {
        myNeighbors.put(pDirection, r);
    }
    
    /**
     * Return the adjacent room in the requested direction.
     * Return null if there is no Room in that direction.
     */
    public Room getNeighbor(String pDirection)
    {
        return myNeighbors.get(pDirection);
    }
    
    /**
     * Remove and return the room’s item(s). 
     */
    public ArrayList<Item> removeItem()
    {
        ArrayList<Item> temp = item;
        item = null;
        return temp;
    }
    
    /**
     * return a String that begins with "You are" followed 
     * by the room description. If there is an item in the Room, 
     * also include "You see" followed by the item’s name.
     */
    public String getLongDescription()
    {
        String temp = "You are " + roomDescription;
        if(hasItem())
        {
            temp += "You see " + item.get(0).getName();
            if(item.size() == 2)
                temp += " and " + item.get(1).getName();
            temp += ".";
        }
        return temp;
    }
}