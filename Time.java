import java.text.DecimalFormat;
/**
 * Creates object of Time where
 * 0.0 = Day 1, noon
 * 0.05 = Day 1, 5 PM
 * 0.12 = Day 2, 12 AM
 * 1.01 = Day 2, 1 AM
 * and so forth.
 * Game stops once Time is 1.8 and because of
 * this the methods in this class will no longer
 * function properly after t = 1.12
 * 
 * @author Kyle Hekhuis 
 * @version Fall 2014
 */
public class Time
{
    // Instantiate instance variable
    private double t;
    
    /**
     * Creates an object of Time
     * parameter double t is initial time.
     * Needs to be between 0.01 and .07 (inclusive)
     * at increments of hundredths.
     */
    public Time(double t)
    {
        this.t = t;
    }
    
    public double getTime()
    {
        return (double) Math.round(t * 100) / 100;
    }
    
    /**
     * Adds an hour to the time
     */
    public void addTime()
    {
        if(getTime() == .12)
            t = 1.01;
        else
        {
            t += .01;
            t = (double) Math.round(t * 100) / 100;
        }
    }
    
    public String toString()
    {
        String temp = Double.toString(getTime());
        String time = temp.substring(2,temp.length());
        if(getTime() == .1)
            time += "0";
        if(getTime() < .12)
            time += " P.M.";
        else
            time += " A.M.";
        return time;
    }
}